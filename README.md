# KTIJ-104 Reproducible Example

See https://gitlab.com/xdevs23/ktij-104-example/-/blob/main/src/main/kotlin/ExampleMap.kt

Affected functions:
- `ExampleMap<K, V>::put(Any?)`
- `ExampleMapSet<E>::add(List<E>, Boolean)`
- `ExampleMapSet<E>::add(E)`

It appears like functions only used inside the classes are affected.
Unused and overridden functions are not affected.

https://youtrack.jetbrains.com/issue/KTIJ-104?u=1