import java.util.*

public class ExampleMap<K, V> : AbstractMutableMap<K, V>() {

    private val list = LinkedList<Any?>()

    override val entries: MutableSet<MutableMap.MutableEntry<K, V>>
        get() = TODO("Not yet implemented")

    public fun put(unrelated: Any?) {
        // It says this function could be private even though I want
        // it to be a public API.
        list.add(unrelated)
    }

    override fun put(key: K, value: V): V? {
        // This one is fine
        put(key)
        return value
    }

}

public class ExampleMapSet<E> {

    public fun add(elements: List<E>, foo: Boolean) {
        // Same here, this one is supposed to be part of the public API
    }

    public fun add(element: E): Boolean {
        add(listOf(element), true)
        return true
    }

    public fun addAll(elements: Collection<E>): Boolean {
        return elements.all { add(it) }
    }

    public fun clear() {
        TODO("Not yet implemented")
    }

    public fun iterator(): MutableIterator<E> {
        TODO("Not yet implemented")
    }

    public fun remove(element: E): Boolean {
        TODO("Not yet implemented")
    }

    public fun removeAll(elements: Collection<E>): Boolean {
        TODO("Not yet implemented")
    }

    public fun retainAll(elements: Collection<E>): Boolean {
        TODO("Not yet implemented")
    }

    public val size: Int
        get() = TODO("Not yet implemented")

    public fun contains(element: E): Boolean {
        TODO("Not yet implemented")
    }

    public fun containsAll(elements: Collection<E>): Boolean {
        TODO("Not yet implemented")
    }

    public fun isEmpty(): Boolean {
        TODO("Not yet implemented")
    }

}
